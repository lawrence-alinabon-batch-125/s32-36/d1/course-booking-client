let addCourseForm = document.querySelector('#createCourse');

addCourseForm.addEventListener("submit", (e) => {
    
    e.preventDefault();

    let courseName = document.querySelector('#courseName').value;
    let coursePrice = document.querySelector('#coursePrice').value;
    let courseDesc = document.querySelector('#courseDesc').value;

    if (courseName === "" || coursePrice === "" || courseDesc === "") {
        alert(`Please input require fields `)
    } else {

        let token = localStorage.getItem('token');
        fetch('http://localhost:3000/api/courses/addCourse',
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization" : `Berear ${token}`
                },
                body: JSON.stringify({
                    name : courseName,
                    price: coursePrice,
                    description: courseDesc
                })
            }
        )
        .then(result => result.json())
        .then(result => {
             if (result == true){
                alert("Course Added Successfully")
                window.location.replace('./courses.html')
            }else{
                alert("Course Not Added. Something went wrong")
            }
        })
    }

})