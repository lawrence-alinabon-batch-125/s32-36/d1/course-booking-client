//target the form
    // getElementById - no need hash to select registerid
    // getElementByClassName
    // getElementByTagName

    // querySelector # for id, . for class
let registerForm = document.querySelector('#registerUser');

// add event Listener
registerForm.addEventListener("submit", (e) => {

    //  para hindi magload ang page
    e.preventDefault();
    // target the element of the form and get their values

    let firstName = document.querySelector('#firstName').value;
    let lastName = document.querySelector('#lastName').value;
    let mobileNo = document.querySelector('#mobileNo').value;
    let email = document.querySelector('#email').value;
    let password = document.querySelector('#password').value;
    let password2 = document.querySelector('#password2').value;

    if( password === password2 && mobileNo.length === 11 && password !== "" && password2 !== ""){
        // fetch(<url>, {options}).then()
        fetch("http://localhost:3000/api/users/checkEmail", 
            {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body:JSON.stringify({
                    email : email
                })
            }
        )
        .then ( result => result.json())
        .then (result => {
            // result === false
                // means this can be save in the database
            
            if(result === false){
                fetch( "http://localhost:3000/api/users/register",
                    {
                        method: "POST" ,
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify({
                            firstName : firstName,
                            lastName : lastName,
                            mobileNo : mobileNo,
                            email : email,
                            password : password
                        })
                    }
                )
                .then( result => result.json() )
                .then( result => {
                    if (result == true){
                        alert("Registered Successfully")
                        window.location.replace('./login.html')
                    }else{
                        alert("Register Failed. Something went wrong")
                    }
                } ) 
            }else{
                alert("Email already exist")
            }
        });
    }  

});