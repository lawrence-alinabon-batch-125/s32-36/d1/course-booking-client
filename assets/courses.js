
let token = localStorage.getItem('token');
let adminUser = localStorage.getItem('isAdmin') === "true";
let adminButton = document.querySelector('#adminButton');

// console.log(adminUser)
if (adminUser === false || adminUser === null) {
    console.log(adminUser)
    adminButton.innerHTML = null
} else {

    adminButton.innerHTML =
        `
            <div class="col-md-2 offset-md-10">
                <a href="./addCourse.html" class="btn btn-block btn-primary">
                    Add Course
                </a>
            <div
        `
    fetch('http://localhost:3000/api/courses/all',
        {
            method: "GET" ,
            header: {
                "Authorization" : `Bearer ${token}`
            }
        }
    )
    .then(result => result.json())
    .then(result => {
        let courseData;

        if (result.lenght < 1) {
            courseData = `No Courses Available`
        } else {
            courseData = result.map((course) => {
                console.log(course);

                return (
                    `
                        <div class="col-md-6 my-5">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">
                                        ${course.name}
                                    </h5>
                                    <p class = "card-text text-left">
                                        ${course.description}
                                    </p>
                                    <p class = "card-text text-right">
                                        &#8369; ${course.price}
                                    </p>
                                </div>
                                <div class = "card-footer">
                                </div>
                            </div>
                        </div>
                    `
                )
            }).join('');
        }

        let container = document.querySelector('#courseContainer')
        container.innerHTML = courseData;
    })
}